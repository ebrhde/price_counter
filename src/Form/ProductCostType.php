<?php

namespace App\Form;

use App\Entity\Product;
use App\Validator\Constraints\TaxCodeLength;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Regex;

class ProductCostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'choice_label' => 'title'
            ])
            ->add('taxCode', TextType::class, [
                'constraints' => [
                    new Regex('/^(DE|IT|GR)\d+$/'),
                    new TaxCodeLength()
                ],
                'attr' => array(
                    'placeholder' => 'e.g. DE000000000'
                )
            ])
            ->add('count', SubmitType::class)
        ;
    }
}
