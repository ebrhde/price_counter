<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $testData = [
            [
                'title' => 'Samsung Galaxy S22',
                'price' => '1299'
            ],
            [
                'title' => 'iPhone 14 256GB',
                'price' => '1549'
            ],
            [
                'title' => 'AirPods 3 Earphones',
                'price' => '249'
            ],
            [
                'title' => 'OtterBox Samsung Galaxy S22 Case',
                'price' => '59'
            ],
        ];

        foreach ($testData as $testDatum) {
            $product = new Product();
            $product->setTitle($testDatum['title']);
            $product->setPrice($testDatum['price']);

            $manager->persist($product);
        }

        $manager->flush();
    }
}
