<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Persistence\ObjectManager;

class CountryFixtures extends \Doctrine\Bundle\FixturesBundle\Fixture
{

    public function load(ObjectManager $manager): void
    {
        $testData = [
            [
                'name' => 'Germany',
                'symbol' => 'DE',
                'tax_amount' => 19,
                'tax_code_length' => 11
            ],
            [
                'name' => 'Italy',
                'symbol' => 'IT',
                'tax_amount' => 22,
                'tax_code_length' => 12
            ],
            [
                'name' => 'Greece',
                'symbol' => 'GR',
                'tax_amount' => 24,
                'tax_code_length' => 11
            ],
        ];

        foreach ($testData as $testDatum) {
            $country = new Country();
            $country->setName($testDatum['name']);
            $country->setSymbol($testDatum['symbol']);
            $country->setTaxAmount($testDatum['tax_amount']);
            $country->setTaxCodeLength($testDatum['tax_code_length']);

            $manager->persist($country);
        }

        $manager->flush();
    }
}