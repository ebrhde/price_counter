<?php

namespace App\Entity;

use App\Enum\Status;
use App\Repository\CountryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
class Country
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: Types::STRING,length: 255)]
    private string $name;

    #[ORM\Column(type: Types::STRING,length: 2)]
    private string $symbol;

    #[ORM\Column(name: 'tax_amount', type: Types::INTEGER, length: 2)]
    private int $taxAmount;

    #[ORM\Column(name: 'tax_code_length', type: Types::INTEGER, length: 2)]
    private int $taxCodeLength;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getTaxAmount(): int
    {
        return $this->taxAmount;
    }

    public function setTaxAmount(int $taxAmount): self
    {
        $this->taxAmount = $taxAmount;

        return $this;
    }

    public function getTaxCodeLength(): int
    {
        return $this->taxCodeLength;
    }

    public function setTaxCodeLength(int $taxCodeLength): self
    {
        $this->taxCodeLength = $taxCodeLength;

        return $this;
    }
}
