<?php

namespace App\Service\Product;

use App\Entity\Product;
use App\Service\Country\CountryProvider;

class PriceCalculator
{
    public function __construct(private readonly CountryProvider $provider)
    {

    }

    public function handle(Product $product, string $taxCode): ?int
    {
        $countrySymbol = substr($taxCode, 0, 2);

        if(!$country = $this->provider->getCountry($countrySymbol)) {
            return null;
        }

        return $this->calculateTotalCost($product->getPrice(), $country->getTaxAmount());
    }

    private function calculateTotalCost(int $productPrice, int $taxAmount): ?int
    {
        return ceil($productPrice + $productPrice / 100 * $taxAmount);
    }
}