<?php

namespace App\Service\Country;

use App\Entity\Country;
use App\Repository\CountryRepository;

class CountryProvider
{
    /**
     * @var Country[] $_countryStorage
     */
    private array $_countryStorage;

    public function __construct(private readonly CountryRepository $repository)
    {

    }

    public function getCountry(string $countrySymbol): ?Country
    {
        if(!isset($this->_countryStorage[$countrySymbol])) {
            $this->_countryStorage[$countrySymbol] = $this->repository->findOneBySymbol($countrySymbol);
        }

        return $this->_countryStorage[$countrySymbol] ?? null;
    }
}