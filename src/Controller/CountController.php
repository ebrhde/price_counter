<?php

namespace App\Controller;

use App\Form\ProductCostType;
use App\Service\Product\PriceCalculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CountController extends AbstractController
{
    #[Route('/', name: 'app_site', methods: ['GET', 'POST'])]
    public function index(Request $request, PriceCalculator $service): Response
    {
        $form = $this->createForm(ProductCostType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $product = $data['product'];
            $taxCode = $data['taxCode'];

            $result = $service->handle($product, $taxCode);

            return $this->render('calculation/result.html.twig', [
                'status' => $result ? 'success' : 'fail',
                'result' => $result,
                'product' => $product->getTitle(),
                'tax_code' => $taxCode
            ]);
        }

        return $this->render('calculation/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
