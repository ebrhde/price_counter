<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class TaxCodeLength extends Constraint
{
    public string $message = "Customer's personal tax code for {{ country }} should contain {{ length }} characters including country symbol";

    public function validatedBy(): string
    {
        return get_class($this).'Validator';
    }
}