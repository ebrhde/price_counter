<?php

namespace App\Validator\Constraints;

use App\Service\Country\CountryProvider;
use Symfony\Component\Validator\Constraint;

class TaxCodeLengthValidator extends \Symfony\Component\Validator\ConstraintValidator
{
    public function __construct(private readonly CountryProvider $provider)
    {
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint): void
    {
        if(null === $value || '' === $value) {
            return;
        }

        $countrySymbol = substr($value, 0, 2);

        if(!$country = $this->provider->getCountry($countrySymbol)) {
            return;
        }

        $countryName = $country->getName();
        $taxCodeLength = $country->getTaxCodeLength();

        if ($taxCodeLength !== strlen($value)) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ country }}', $countryName)
                ->setParameter('{{ length }}', $taxCodeLength)
                ->addViolation();
        }
    }
}